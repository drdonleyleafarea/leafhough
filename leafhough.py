# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 22:18:10 2015

@author: Tom
Based on code from Vinnie Monaco: https://github.com/vmonaco/general-hough
"""


import os
import numpy as np
import matplotlib.pyplot as plt
import mahotas as mh
from progressbar import ProgressBar, Bar, Percentage
import cv2
from collections import defaultdict
from scipy.ndimage import imread as sp_imread
from scipy.misc import imresize
from scipy.ndimage.filters import sobel
from skimage import img_as_float, draw
from skimage import transform as tf
#from skimage.draw import ellipse_perimeter
#from skimage.feature import canny
#from skimage.filter import scharr, prewitt, roberts
#from skimage.transform import hough_ellipse
from operator import itemgetter
#from cv2 import cornerHarris
from leafarea import *

#ANN
#import pybrain as pb






# Good for the b/w test images used
r_table_min_threshold = 0.75
r_table_max_threshold = 2
accum_min_threshold = 0.2
accum_max_threshold = 0.5
se = np.ones((3,3))    

    
def get_scale(leafimage, minticks, scaleunits):
    print "get_scale"
    n, m = leafimage.shape
    
    scale, found, edge = metricscale(leafimage, minticks, scaleunits)
#        print "width,height (cm) = ", n * scale, m * scale, "scale = ", scale
    if not found:   # try to find a scale after histogram equalization
        scale, found, edge = metricscale(leafimage, minticks, scaleunits, True, False)
#            print "width,height with histogram equalization (cm) = ", n * scale, m * scale, "scale = ", scale

    if not found:   # try to find a scale after Gaussian blur
        scale, found, edge = metricscale(leafimage, minticks, scaleunits, False, True)
#                print "width,height with histogram equalization (cm) = ", n * scale, m * scale, "scale = ", scale
    return scale
    

def iso_leaf(ref_image, leaf_num):
    print "iso_leaf"
    # Input:
    #   ref_image: image of leaf to be scanned
    #   leaf_num: number of leaf to be isolated from ref_image
    # Output:
    #   petiole: image cropped to the dimensions of the petiole of interest
    # Finds the leaf specified by leaf_num in ref_image and returns an image
    # cropped to the smallest box capable of bounding said leaf.
    petiole = np.copy(ref_image)
    
    # Isolate the petiole by comparing pixel value to leaf_num and creating a
    # list of indeces where the petiole resides
    pixels = petiole[:,:] != leaf_num
    petiole[pixels] = 0
    leaf = []
    for index, pixel in np.ndenumerate(pixels):
        if not pixel:
            leaf.append(index)
    
    # Get the pixel locations of the top, bottom, left, and right edges of
    # the petiole and pad the bounding box by 1 pixel.
    bottom = max(leaf, key=itemgetter(0))[0] + 1
    top = min(leaf, key=itemgetter(0))[0] - 1
    left = min(leaf, key=itemgetter(1))[1] -1
    right = max(leaf, key=itemgetter(1))[1] + 1
    petiole = petiole[top:bottom,left:right]

#    print "Top: %r, Bottom: %r" %(top, bottom)
#    print "Left: %r, Right: %r" %(left, right)
#    plt.imshow(petiole)
#    plt.show()
    return petiole


def general_hough_closure(reference_image):
    '''
    Generator function to create a closure with the reference image and origin
    at the center of the reference image
    
    Returns a function f, which takes a query image and returns the accumulator
    '''
    print "general_hough_closure"
    referencePoint = (reference_image.shape[0]/2, reference_image.shape[1]/2)
#    print referencePoint
    r_table = build_r_table(reference_image, referencePoint)
    
    def f(query_image):
        return accumulate_gradients(r_table, query_image)
    return f


def build_r_table(image, origin, polar=True, cornering=False):
    '''
    Build the R-table from the given shape image and a reference point on the
    reference image
    '''
    print "build_r_table"   
#    cornering=True
    polar = False
    # Create an edge image by eroding and then subtracting the foreground        
    edges = image-mh.morph.erode(image, se)
    edges = normalize(edges)
    # Calculate Gradient Orientation in Degrees
    gradient = gradient_orientation(edges)
    # Corners
#    corners = 0
#    if cornering:
#        corners = cv2.cornerHarris(image.astype(np.float32), 6, 3, 0.04)
#        corners = normalize(corners)
#        plt.imshow(corners)
#        plt.show()

    
    # Create Empty R-Table Dictionary
    r_table = defaultdict(list)
    
#    minvalue = np.min(edges)
    for (i,j), value in np.ndenumerate(corners if cornering else edges):
         if value != 0:
             '''
             If the pixel is an edge, append the x,y relationship between the
             pixel and the origin to the r-table dictionary whose key is the
             gradient direction of the edge
             The offset of all pixels with a particular gradient orientation
             are stored:
             gradient direction: [(pixel1_x, pixel1_y), (pixel2_x, pixel2_y),...]
             '''
             if polar:
                 r = np.sqrt(float(i-origin[0])**2.+float(j-origin[1])**2.)
                 alpha = np.arctan2(float(origin[0]-i), float(origin[1]-j))
                 r_table[gradient[i,j]].append((r, alpha))
             else:
                 r_table[gradient[i,j]].append((origin[0]-i, origin[1]-j))
    return r_table


def gradient_orientation(image):
    '''
    Calculate the gradient orientation for edge point in the image
    '''
    print "gradient orientation"
    dx = sobel(image, axis=0, mode='constant')
    dy = sobel(image, axis=1, mode='constant')
    gradient = np.arctan2(dy, dx)    
    return gradient

def normalize(image, weight = 0.5):
    minvalue = np.min(image)
    maxvalue = np.max(image)    
    thresh = weight*(maxvalue-minvalue)+minvalue
    
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            if image[i,j] >= thresh:
                image[i,j] = 1
            else:
                image[i,j] = 0
    return image.astype(int)

def outline(image, alignment='x', indexing = 'coord'):
    #coord, ravel, unravel    
    # USE DEFAULTDICT TO AVOID DUPLICATES
    pixels = [[], []]
    image = normalize(image)
    if alignment == 'x':
        for i in range(image.shape[0]):
            for j in range(image.shape[1]):
                if image[i,j] != 0:
                    pixels[1].append(i)
                    pixels[0].append(j)
                    break
            for j in range(image.shape[1]):
                if image[i,-j] != 0:
                    pixels[1].append(i)
                    pixels[0].append(image.shape[1]-j)
                    break
    if alignment == 'y':
        for j in range(image.shape[1]):
            for i in range(image.shape[0]):
                if image[i, j] != 0:
                    pixels[1].append(i)
                    pixels[0].append(j)
                    break
            for i in range(image.shape[0]):
                if image[-i, j] != 0:
                    pixels[1].append(image.shape[0]-i)
                    pixels[0].append(j)
                    break
    plt.scatter(pixels[0][:], pixels[1][:])
    plt.show()
    if indexing == 'coord':
#        print pixels[:2]      
        return pixels
    elif indexing == 'ravel':
#        print pixels[0][:2], pixels[1][:2]
        pixels = np.ravel_multi_index(pixels, image.shape)
#        print pixels[:2]
        return pixels

def accumulate_gradients(r_table, grey_image, angles=np.linspace(-np.pi, np.pi, 10), scales=np.linspace(-np.pi, np.pi, 10), polar=True, normalizing=True, cornering=False, gradient_weight=False, show_progress=True, verbose=True):
    '''
    Perform a General Hough Transform with the given image and R-table
    grey_image is the query image
    '''
    print "accumulate_gradients"
    polar = False
#    gradient_weight=True
#    cornering=True
    query_edges = grey_image-mh.morph.erode(grey_image, se)
#    query_corners = cv2.cornerHarris(grey_image.astype(np.float32), 6, 3, 0.04)
#    plt.imshow(query_corners)
#    plt.show()
    
    if normalizing:
        query_edges = normalize(query_edges)
#        query_corners = normalize(query_corners)
    print "using gradient_weight" if gradient_weight else "using unity weight"
    
    gradient = gradient_orientation(query_edges)
#    print np.max(gradient)
#    print np.min(gradient)
#    print np.max(query_edges)
    
    # Display a progress bar
    if show_progress:
        scanning_pixels = 0
        for (i,j), value in np.ndenumerate(query_edges):
            if value != 0: scanning_pixels+=1
        print "Edge Pixels: ", scanning_pixels
            
    plt.imshow(query_edges)
    plt.show()
    
    accumulator = np.zeros((grey_image.shape[0], grey_image.shape[1], len(angles), len(scales)))
    pixels_scanned = 0    
    '''
    Net inputs: i, j, theta, rho, i*cos(theta), i*sin(theta), j*cos(theta), j*sin(theta)
    '''
#    progress = ProgressBar(maxval)
#    with ProgressBar() as progress:
    if show_progress: pbar = ProgressBar(widgets=[Percentage(), Bar()], maxval=scanning_pixels).start()        
    for (i,j), value in np.ndenumerate(query_edges):
        if value != 0:
            '''
            If the pixel at (i,j) is an edge then find the pixels with the same
            gradient direction in the r-table.
            accum_i, accum_j is the origin of the reference image on the query
            image
            If the origin of the point is in the image then increment the pixel
            location in the accumulator
            '''
#            print "r_table length"
#            print len(r_table[gradient[i,j]])
            for pixels in r_table[gradient[i,j]]:
                if polar:
                    xp = pixels[0]*np.cos(pixels[1])
                    yp = pixels[0]*np.sin(pixels[1])
                else:
                    xp = float(pixels[1])
                    yp = float(pixels[0])
                for a in range(len(angles)):
                    for s in range(len(scales)):                        
                        accum_j, accum_i = int(float(j)-(xp*np.cos(angles[a])-yp*np.sin(angles[a]))*scales[s]), int(float(i)-(xp*np.sin(angles[a])+yp*np.cos(angles[a]))*scales[s])
                        'NeuroHough Neural Network HERE'
                        if (0 <= accum_i < accumulator.shape[0]) and (0 <= accum_j < accumulator.shape[1]):
                            if gradient_weight:
                                accumulator[accum_i, accum_j, a, s] += 1+gradient[i,j]
                            else:
                                accumulator[accum_i, accum_j, a, s] += 1
            if show_progress:
                pixels_scanned+=1
                pbar.update(pixels_scanned)
    if show_progress: pbar.finish()
    return accumulator, angles, scales


def test_general_hough(gh, reference_image, query_image, path, query_index=0):
    '''
    Uses a GH closure to detect shapes in an image and create nice output
    '''
    print "test_general_hough"
    accumulator, angles, scales = gh(query_image)
    outline(query_image)

    plt.clf()
    plt.gray()
    
    fig = plt.figure(figsize=(32,44))
    fig.add_subplot(2,2,1)
    plt.title('Reference image')
    plt.imshow(reference_image)
    plt.scatter(reference_image.shape[0]/2, reference_image.shape[1]/2, marker='o', color='y')
    
    fig.add_subplot(2,2,2)
    plt.title('Query image')
    plt.imshow(query_image)
    
    fig.add_subplot(2,2,3)
    plt.title('Accumulator')
    i,j,k,l = np.unravel_index(accumulator.argmax(), accumulator.shape)
    plt.imshow(accumulator[:,:, k, l])
    
    fig.add_subplot(2,2,4)
    plt.title('Detection')
    plt.imshow(query_image)
    
    # top 5 results in red
    m = n_max(accumulator, 5)
    x_points = [pt[1][1] for pt in m]
    y_points = [pt[1][0] for pt in m] 
    rots = [pt[1][2] for pt in m]
    scalers = [pt[1][3] for pt in m]
    plt.scatter(x_points, y_points, marker='o', )
    print angles[rots]*180./np.pi
    print scales[scalers]

    # top result in yellow
    plt.scatter([j], [i], marker='o', color='y')
    print "Angle = ", angles[k]*180./np.pi
    print "Scale = ", scales[l]
    print
    
    
    d,f = os.path.split(path)[0], os.path.splitext(os.path.split(path)[1])[0]
    plt.savefig(os.path.join(d, f + '_output_%s_scale%s.png' %(query_index, scales[l])))
    return

    
def n_max(a, n):
    '''
    Return the N max elements and indices in a
    '''
    print "n_max"
    indices = a.ravel().argsort()[-n:]
    indices = (np.unravel_index(i, a.shape) for i in indices)
    return [(a[i], i) for i in indices]

def square_test():
    print "square_test"
    dir = os.path.dirname(__file__)
    ref_im_file = os.path.join(dir, "../images/TEST/triangle.gif")
#    query_im_file = os.path.join(dir, "../images/TEST/query.jpg")
#    ref_im = sp_imread(ref_im_file, flatten=True).astype(int)
#    ref_imcv2 = cv2.imread("../images/TEST/triangle.gif", 0)
#    print ref_imcv2.shape
#    ref_im = int(ref_im[:,:])
#    ref_im = np.ones(ref_im.shape, dtype=int)-ref_im
#    ref_im = 1-ref_im
#    ref_im = normalize(ref_im)
    ref_im = np.zeros((128, 128), dtype=int)
    rr, cc = draw.circle(ref_im.shape[0]/2, ref_im.shape[1]/2, ref_im.shape[0]/4)
#    ref_im = np.zeros(ref_im.shape, dtype=int)
    ref_im[rr, cc] = 1
    ref_im[56:72, 64:112] =1
    ref_im[16:64, 48:72] = 1
    plt.imshow(ref_im)
    plt.show()
#    plt.imshow(circ_ref)
#    im = ndimage.rotate(ref_im, 30)    
#    ref_im = np.zeros((128, 128), dtype=int)
#    ref_im[63:64, 16:-16] = 1
    
#    ref_imcv2 = np.zeros((128, 128), dtype=np.float32)
#    ref_imcv2[32:-32, 16:-16] = 1.
#    grey = cv2.cvtColor(ref_im, cv2.COLOR_BGR2GRAY)
#    dst = cv2.cornerHarris(ref_im.astype(np.float32), 6, 3, 0.04)
#    dst = normalize(dst)
#    plt.imshow(dst)
#    plt.show()
    
#    im = ndimage.rotate(ref_im, 45)
    plt.imshow(ref_im)
    plt.show()
#    corners = ref_im
#    corners[dst>0.01*dst.max()]=1
#    plt.imshow(corners)
#    plt.show()
    tform = tf.SimilarityTransform(scale=1., translation=(-30, 30), rotation=-np.pi/7.)
    im = tf.warp(ref_im, tform)
    im = normalize(im)
    plt.imshow(im)
    plt.show()
#    print im.shape
#    print np.max(ref_im)
    
#    ref_im = normalize(ref_im)
#    plt.imshow(ref_im)
#    plt.show()
    closure = general_hough_closure(ref_im)
    accumulator, angles, scales = closure(im)
    i,j,k,l = np.unravel_index(accumulator.argmax(), accumulator.shape)
    
    fig = plt.figure(figsize=(16,22))
    fig.add_subplot(2,2,1)
    plt.title('Reference image')
    plt.imshow(ref_im)
    plt.scatter(ref_im.shape[0]/2, ref_im.shape[1]/2, marker='o', color='y')
    
    fig.add_subplot(2,2,2)
    plt.title('Query image')
    plt.imshow(im)
    
    fig.add_subplot(2,2,3)
    plt.title('Accumulator')
    plt.imshow(accumulator[:,:, k, l])
    
    fig.add_subplot(2,2,4)
    plt.title('Detection')
    plt.imshow(im)
#    plt.imshow(imresize(ndimage.rotate(ref_im, angles[k]*180./np.pi), scales[l]))
#    plt.scatter(ref_im.shape[0]*scales[l]/2, ref_im.shape[1]*scales[l]/2, marker='o', color='y')
    

    
    # top 5 results in red
    m = n_max(accumulator, 5)
    x_points = [pt[1][1] for pt in m]
    y_points = [pt[1][0] for pt in m]
    rots = [pt[1][2] for pt in m]
    scalers = [pt[1][3] for pt in m]
    plt.scatter(x_points, y_points, marker='o', color='r')
    print x_points
    print y_points
    print angles[rots]*180./np.pi
    print scales[scalers]

    # top result in yellow
    plt.scatter(j, i, marker='o', color='y')
    plt.show()
    print "Angle = ", angles[k]*180./np.pi
    print k
    print "Scale = ", scales[l]
    print
    outline(ref_im, alignment='y')
    
#    test_general_hough(closure, ref_im, im, query_im_file, query_index=1)
    
    
def test():
    print "test"
    minticks = 10
    scaleunits = 'mm'
    minsegmentarea = 0.1
    itermagnification = 2
    debug = False
    width, height = 0, 0 #dimensions of grey_image
    
    # Read Images
    dir = os.path.dirname(__file__)
    ref_im_file = os.path.join(dir, "../images/TEST/reference.jpg")
    query_im_file = os.path.join(dir, "../images/TEST/query.jpg")
    
    grey_ref = sp_imread(ref_im_file, flatten=True)
    color_ref = sp_imread(ref_im_file)
    

    grey_query = sp_imread(query_im_file, flatten=True)
    color_query = sp_imread(query_im_file)
    
    grey_hpixels, grey_wpixels = grey_ref.shape
    
#    print grey_ref.shape
#    print grey_wpixels
#    print grey_hpixels
    
    
    # Get image size properties
    if width==0 or height==0:
        grey_scale = get_scale(grey_ref, minticks, scaleunits)
    else:  # width and height in cm specified by user, don't need to calculate scale
        found = True
        # The scale should be the same calculated from the width or the height, but average the two,
        # just in case there is some small discrepency.
        grey_scale = (width/float(n) + height/float(m)) * 0.5
    
    segmented_ref = segment(sp_imread(ref_im_file, flatten=True), sp_imread(ref_im_file), ref_im_file, 50, itermagnification=2, debug=False, scale=grey_scale, minsegmentarea=0.1, datadir="./")
    segmented_query = segment(sp_imread(query_im_file, flatten=True), sp_imread(query_im_file), query_im_file, 50, itermagnification=2, debug=False, scale=grey_scale, minsegmentarea=0.1, datadir="./")

    plt.imshow(segmented_ref[0])    
    plt.show()
    plt.imshow(segmented_query[0])
    plt.show()
#    segmented_ref[0] = imresize(segmented_ref[0], 7)
#    segmented_query[0] = imresize(segmented_query[0], 7)
#    cropped_ref = segmented_ref[0][:, :-300]
    
    query_leaflets = []
    ref_leaflets = []
    # Isolate the individual leaflets in the ref and the query images
    for i in range(segmented_ref[3]):
#        print iso_leaf(segmented_ref[0], i+1).shape
        ref_leaflets.append(imresize(iso_leaf(segmented_ref[0], i+1), 7))
    outline(ref_leaflets[0])    
#        print "Max/Min ref_leaflets[%r]" %i        
#        print np.max(ref_leaflets[i])
#        print np.min(ref_leaflets[i])
#        print ref_leaflets[i].shape

    for i in range(segmented_query[3]):
        # Iterate up to numleaves
        query_leaflets.append(imresize(iso_leaf(segmented_query[0], i+1), 7))
#        print query_leaflets[i].shape
        leaf_accumulator = general_hough_closure(ref_leaflets[0]) #gh function
        test_general_hough(leaf_accumulator, ref_leaflets[0], query_leaflets[i], query_im_file, query_index=i)

    
if __name__ == '__main__':
#    test()
    square_test()
    